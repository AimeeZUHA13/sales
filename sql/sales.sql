CREATE SCHEMA stg;

SET search_path TO stg;


CREATE TABLE IF NOT EXISTS stg.customer (
    pk_customer SERIAL PRIMARY KEY
	,customer_first_name VARCHAR(100) NOT NULL
	,customer_last_name VARCHAR(100) NOT NULL
	,customer_email VARCHAR(100) 
	,customer_phone VARCHAR(40)
	,city VARCHAR(100) 
	,country VARCHAR(100) 
	,postal_code VARCHAR(40) 
	,national_id_number VARCHAR(25) 
	,tax_id_number VARCHAR(20) 
);

CREATE TABLE IF NOT EXISTS stg.item (
    pk_item SERIAL PRIMARY KEY
	,item_name VARCHAR(100) NOT NULL
	,item_price NUMERIC
);

CREATE TABLE IF NOT EXISTS stg.item_purchase (
    pk_purchase SERIAL PRIMARY KEY
	,purchase_order_number VARCHAR(100)
	,purchase_date TIMESTAMP DEFAULT NOW()
	,purchase_comment TEXT
	,fk_customer INTEGER REFERENCES stg.customer(pk_customer)
	,fk_item INTEGER REFERENCES stg.item(pk_item)
);

INSERT INTO stg.customer
(customer_first_name, customer_last_name, customer_email, customer_phone, city, country, postal_code, national_id_number, tax_id_number)
VALUES
('Esteban', 'Restrepo', 'esteban.restrepo@hotmail.com', '3319257830', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'JIOA770826MCSMRD51', 'JIOA770826M22'),
('Nelson', 'Nieves', 'nelson.nieves@hotmail.com', '8120407461', 'LA MAGDALENA CONTRERAS', 'MX', '67890 ', 'NIAN72925HGRVRL82', 'NIAN72925H76'),
('Cassandra', 'Morales', 'cassandra.morales@gmail.com', '+5233232341', 'COYOACAN', 'MX', '04918 ', 'MOHC801121MMSRRS34', 'MOHC801121M18'),
('Denisse Guadalupe', 'Pichardo', 'denisse.pichardo@gmail.com', '58034958349', 'QUERETARO', 'MX', '76000 ', 'PIGD9341MCXCZN47', 'PIGD9341M94'),
('Nathali', 'Lara', 'nathali.lara@yahoo.com', '+55115683517', 'CUAUTITLAN IZCALI', 'MX', '54700 ', 'LAUN0165MCXRRTW2', 'LAUN0165M84');

INSERT INTO stg.item
(item_name, item_price)
VALUES
('Cheddar cheese', 22.03),
('Condensed milk', 11.45),
('Instant chocolate', 19.95),
('Tomato ketchup ', 9.95),
('Macaroni', 4.95),
('Eggs', 17.95),
('Seltzer Water', 2.99),
('Olive Oil ', 3.37),
('Avocados', 1.10),
('Chicken Sausage', 3.99);

INSERT INTO stg.item_purchase
(purchase_order_number, purchase_comment, fk_customer, fk_item)
VALUES
('PO-AV93QTG4', 'GREAT INDEPENDENT GROCERY STORE WITH A WIDE VARIETY OF GOURMET ITEMS AND HARD TO FIND FOOD STUFFS.', 1, 4),
('PO-DNDZDX6T', 'The best bulk selection around. Organic, beautiful produce. ', 3, 2),
('PO-CCXDF75A', 'THIS IS THE BEST LOCAL INDEPENDENTLY OWNED HEALTH FOOD STORE IN THE COUNTY', 2, 1),
('PO-EPVS3N3Y', 'Amazing selections of produce and deli food. Were weekly shoppers often finding everything from vegan meals and products to grass Fed organic meats and whole grain pasta.', 5, 10),
('PO-BD595ZTK', 'Best store of its type in the area', 4, 8),
('PO-3G677PJL', 'They have a great selection of products. The service staff is very helpful and pleasant.',  2, 7),
('PO-F3Q2Z7UE', ' SERVICE CAN BE SOMETIMES SLOW.', 1, 6),
('PO-A2ESB8AP', 'Fun an quirky place to get some quality food. Love this local establishment!', 3, 5),
('PO-8D5LLZQC', 'FAST SERVICE IS GREAT, BUT RUSHED SERVICE.', 2, 4),
('PO-V867NE83', 'NICE STAFF, INTERESTING AND DELICIOUS FOODS.', 5, 9);
