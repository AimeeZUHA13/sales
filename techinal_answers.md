### What is Data Engineering?

Data engineering, is a discipline of building and maintaining data-based systems. The work of data engineering ensures that data is harvested, inspected for quality, and readily accessible by appropriate data professionals throughout the organization. 

### What are the main responsibilities of a Data Engineer?

It may depend on the scope of the project but the basic responsibilities are:

- Data Acquisition

- Data Modelling

- Data Preparation

- Building Data Pipeline Systems 

1. Data Acquisition
We must first gather data from the appropriate sources.

2. Data Modelling
This begins with gathering data requirements, such as how long data must be maintained, how it will be utilized, and who and what systems must have access to it. 

3.  Data Preparation
We need to verify that the data is complete, cleaned, and other outlier rules have been defined before the data modelings/analyst can construct Data Models.

4. Building Data Pipeline Systems

We are in charge of designing and producing scalable ETL pipelines from business source systems and developing ETL processes for populating Databases and creating aggregates.
During the extraction phase, we must ensure that the ETL pipeline is resilient enough to keep running in the face of unexpected or malformed data, sources getting unavailable or going offline, and fatal bugs. Maintaining uptime is critical, especially for organizations that rely on real-time or time-sensitive data. 

### Explain ETL

An ETL is a type of data pipeline.
Technically, ETL refers to a process in which data is extracted from data sources, transformed to fit or support business needs, and then loaded into the final location.

### How you build a Data Pipeline?.

The following points should be taken into consideration when building a data pipeline:

- Origin:  the point of entry for data from all data sources in the pipeline. 

- Destination: This is the final point to which data is transferred. The final destination depends on the use case. The destination is a Data Warehouse, Data Lake, or Data Analysis and Business Intelligence tool for most use cases.

- Dataflow: This refers to the movement of data from origin to destination, along with the transformations that are performed on it.

- Storage: Storage refers to all systems that are leveraged to preserve data at different stages as it progresses through the pipeline.

- Monitoring: The goal of monitoring is to ensure that the Pipeline and all its stages are working correctly and performing the required operations.

- Technology: These are the infrastructure and tools behind Data Flow, Processing, Storage, Workflow, and Monitoring. 

Example:
I was dedicated to create the pipeline with Pentaho for the ecommerce process of a certain retail company. Those pipelines processes what they did was to carry out the whole process of the sale of an item, from the moment it is made until it is delivered to the customer and invoiced.

### In a RDBMS Joins are your friends.  Explain Why.

The SQL "experts" often disagree on the SQL JOINs, the truth is that in a system that is always relational, there is no better way than to realize those relationships using joins.
Technically speaking the main advantage of a join is that it executes faster. The main advantage of a join is that it executes faster. The performance increase might not be noticeable by the end user. However, because the columns are specifically named and indexed and optimised by the database engine, the retrieval time almost always will be faster than that of a subquery for example.

### What are the main features of a production pipeline.

Technically speaking

- Fault tolerance: ability to survive uncaught exceptions 
- Data redundancy: never lose user data 
- Scalability: Handling extra load should not require re-writing the pipeline 
- Test Coverage: a "decent" amount of pipeline tested 

Personally speaking

"Production-quality pipeline" is whatever another user, who is not you, is...

1. able to use with no or minimal support. 

2. able to understand how to use with minimal support or documentation.

3. willing to use because it adds value. 

### How do you monitor this data pipelines?

Personally speaking, depending on the orchestrator we use.

AWS Step Functions

Inside the step function in each step that is necessary, I place an error handling that if at a certain moment it occurs, a notification is sent via slack/email.

AWS Airflow

Depending on the pipeline, but generally I place in each operator an on_failure_callback that automatically sends to a slack hook when it fails.

### Give us a situation where you decide to use a NoSQL database instead of a relational database. Why did you do so?

The only NoSQL database I have used is MongoDB/Dynamo, I decided to use it to store geospatial data.

This was mainly due to its ease of use, the efficiency with which it operates when performing queries, and the capacity and variety of types of indexes it provides.

### What are the non technical soft skills that are most valuable for data engineers?

For me, soft skills are even more important than hard skills, why? Because personal qualities are more difficult to acquire and learn.

1. Communication
2. Teamwork
3. Organization
4. Problem solving

### Differences between OLAP and OLTP Systems

|                | OLTP        | OLAP                         |
|--------------- |-----------  | ------------------------------------ |
| Characteristics|Handles a large number of small transactions      | Handles large volumes of data with complex queries  |
|Purpose         |Control and run essential business operations in real time | Plan, solve problems, support decisions, discover hidden insights|
|Database design |Normalized databases for efficiency | Denormalized databases for analysis|
