### Data Sources

Data sources can come from any system.

### Data Stream

I choose Confluent Kafka + ksql to find those patterns of interest(noise).

Confluent Kafka:

- Source/Sink Connectors.

This so-called Sources import data into Kafka, and Sinks export data from Kafka. An implementation of a Source or Sink is a Connector. And users deploy connectors to enable data flows on Kafka. Kafka Connect is designed for large-scale data integration and has a built-in parallelism model.

- Schema Registry

Provides a serving layer for our metadata. It provides a RESTful interface for storing and retrieving your Avro, JSON, and Protobuf schemas. It stores a versioned history of all schemas based on a specified subject name strategy, provides multiple compatibility settings and allows evolution of schemas according to the configured compatibility settings and expanded support for these schema types.

- ksqlDB

Is the streaming SQL engine for Kafka. It provides an easy-to-use yet powerful interactive SQL interface for stream processing on Kafka, without the need to write code in a programming language such as Java or Python. ksqlDB is scalable, elastic, fault-tolerant, and real-time. It supports a wide range of streaming operations, including data filtering, transformations, aggregations, joins, windowing, and sessionization.

This component is the main reason for choosing Kafka as the platform that will identify these anomalies, because of its windowing function.<br>
A window has a start time and an end time. Windowing lets you control how to group records that have the same key for stateful operations, like aggregations or joins, into time spans. When we use windows in our ksql queries, aggregate functions are applied only to the records that occur within a specific time window.<br>

This functionality will allow us to group in time windows the anomalies occurred in a certain period grouping them by the time in which they occurred, this together with the near real time stream that Kafka offers, make this platform, the most eligible for me.

### Data Lake

All data sources that go beyond Kafka's topics will be loaded here.<br>
I use an s3 bucket for scalability and ease of integration.

### DWH

Here we can find the final data that will make up the various dims and facts that are required.

### Data Visualization

For greater visibility of patterns of interest, you can create dashboards to visualize the necessary KIPs in your data visualization tool of choice (Power BI, Tableau, Looker, OBIEE, etc).
